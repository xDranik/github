import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany } from 'ember-data/relationships';

export default Model.extend({
  issues: hasMany('issue'),

  name: attr(),
  fullName: attr(),
  owner: attr(),
  private: attr(),
  description: attr(),
  fork: attr(),
  url: attr(),
  createdAt: attr(),
  updatedAt: attr(),
  pushedAt: attr(),
  homepage: attr(),
  size: attr(),
  stargazersCount: attr(),
  watchersCount: attr(),
  language: attr(),
  hasIssues: attr(),
  hasDownloads: attr(),
  hasWiki: attr(),
  hasPages: attr(),
  forksCount: attr(),
  openIssuesCount: attr(),
  forks: attr(),
  openIssues: attr(),
  watchers: attr(),
  defaultBranch: attr(),
  organization: attr(),
  networkCount: attr(),
  subscribersCount: attr(),
  githubId: attr()
});
