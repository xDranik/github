import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  normalize(modelClass, responseHash, prop) {
    responseHash.links = {
      issues: `${responseHash.url}/issues`
    };

    return this._super(modelClass, responseHash, prop);
  }
});
